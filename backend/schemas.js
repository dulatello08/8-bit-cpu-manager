import net from "net";

class UnixSocketClient {
    constructor(socketPath) {
        this.socketPath = socketPath;
        this.client = new net.Socket();
        this.connect();
    }

    connect() {
        this.client.connect(this.socketPath);
    }

    disconnect() {
        this.client.end();
    }

    sendData(data) {
        return new Promise((resolve, reject) => {
            console.log(`Sending data: ${data}`);

            this.client.write(data, 'utf-8', (err) => {
                if (err) {
                    console.error('Error sending data:', err);
                    reject(err);
                } else {
                    console.log('Data sent successfully. Waiting for response...');

                    this.client.once('data', (response) => {
                        const responseData = response.toString('utf-8');
                        console.log(`Received response: ${responseData}`);
                        resolve(responseData);
                    });
                }
            });

            this.client.on('error', (err) => {
                console.error('Socket error:', err);
                reject(err);
            });

            this.client.on('timeout', () => {
                const timeoutError = new Error('Socket communication timed out');
                console.error(timeoutError);
                reject(timeoutError);
            });
        });
    }
}

export default UnixSocketClient