import express from 'express';
import UnixSocketClient from './schemas.js'

const app = express();

const socket = new UnixSocketClient('/tmp/emulator.sock')

app.get('/get', async (req, res) => {
    try {
        const requestData = 'GET'; // The command to send
        const response = await socket.sendData(requestData);
        const jsonResponse = JSON.parse(response);

        // Sending a beautified JSON response to the client
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(jsonResponse, null, 2));
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.listen(1337, () => {
    console.log('Server listening on port 1337');
});